//
//  ViewController.swift
//  ttttt
//
//  Created by Anastasia Zhilinskaya on 14.02.22.
//

import UIKit
/* class Friend: NSObject{
    var name: String?
    var profileImageName : String?
}
class Message: NSObject{
    var text : String?
    var friend : Friend?
} */

class FriendsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
   private let cellId = "cellId"
     var messages : [Message]? 
  /* func setupData() {
        let mark = Friend()
        mark.name = "Item 1"
        mark.profileImageName = "im1"
        
        let message = Message()
        message.friend = mark
        message.text = "Description 1"
        messages = [message]
        
    } */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(MessageCell.self, forCellWithReuseIdentifier: cellId)
    
        setupData() 
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = messages?.count {
        return count
        }
        return 0
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
   /*override func collectionView( collectionView: UICollectionView, cellfor indexPath: NSIndexPath) -> UICollectionViewCell { */
        let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath ) as! MessageCell
        if let message = messages?[indexPath.item]{
            cell.message = message
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
}
class MessageCell: BaseCell {
    var message: Message?{
        didSet{
            
        nameLabel.text = message?.friend?.name
          /* if let profileImageName = message?.friend?.profileImageName {
                profileImageView.image = UIImage(named: profileImageName);
            }
            messageLabel.text = message?.text
} */  }
                                              }
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 34
        imageView.layer.masksToBounds = true
        return imageView
    }()
    let dividerLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor (white: 0.5, alpha: 0.5)
        return view
    }()
    let nameLabel:UILabel = {
        let label = UILabel()
        label.text = "Name"
        return label
    }()
    let messageLabel : UILabel = {
        let label = UILabel()
        label.text = "message"
        label.textColor = UIColor.gray
        return label
      
    }()
   override func setupViews(){
        //backgroundColor = .gray//
       addSubview(dividerLineView)
       addSubview(profileImageView)
       setupContainerView()
       
       profileImageView.image = UIImage( named: "im1")
    
       
       addConstraintsWithFormat(format: "H:|-12-[v0(68)]|", views: profileImageView)
       addConstraintsWithFormat(format: "V:|-12-[v0(68)]|", views: profileImageView)
       addConstraint(NSLayoutConstraint(item: profileImageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
       addConstraintsWithFormat(format: "H:|-0-[v0]|", views: dividerLineView)
    addConstraintsWithFormat(format: "V:|[v0(1)]|", views: dividerLineView)
    }
    private func setupContainerView(){
        let containerView = UIView()
      
        addSubview(containerView)
        addConstraintsWithFormat(format: "H:|-90-[v0]|", views: containerView)
        addConstraintsWithFormat(format: "V:[v0(50)]", views: containerView)
        addConstraint(NSLayoutConstraint(item: containerView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        containerView.addSubview(nameLabel)
        containerView.addSubview(messageLabel)
        containerView.addConstraintsWithFormat(format: "H:|[v0]|", views: nameLabel)
        containerView.addConstraintsWithFormat(format: "V:|[v0][v1(24)]|", views: nameLabel, messageLabel)
        containerView.addConstraintsWithFormat(format: "H:|[v0]|", views: messageLabel)
        
    }
    
}

extension UIView{
    func addConstraintsWithFormat( format : String, views: UIView...){
        var viewsDictionary = [String: UIView]()
        for (index,view) in views.enumerated(){
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
}
class BaseCell: UICollectionViewCell{
    override init(frame: CGRect){
    super.init(frame: frame)
    setupViews()
}
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func setupViews(){
        backgroundColor = .blue
    }
    
}
