//
//  ControllerHelper.swift
//  ttttt
//
//  Created by Anastasia Zhilinskaya on 14.02.22.
//

import UIKit
class Friend: NSObject{
    var name: String?
    var profileImageName : String?
}
class Message: NSObject{
    var text : String?
    var friend : Friend?
}
extension FriendsController {
    func setupData() {
        let mark = Friend()
        mark.name = "Item 1"
        mark.profileImageName = "im1"
        
        let message = Message()
        message.friend = mark
        message.text = "Description 1"
        
        let st = Friend()
        st.name = "Item 2"
        st.profileImageName = "im1"
        
        let messageSt = Message()
        message.friend = st
        message.text = "Description 2"

        let rt = Friend()
        rt.name = "Item 3"
        rt.profileImageName = "im1"
        
        let messageRt = Message()
        message.friend = rt
        message.text = "Description 3"

        
        messages = [message, messageSt, messageRt]
        
    }
}
